﻿using MathGameNova.dll;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Reflection;

namespace MathGameNova.UnitTests
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void When3ReturnsFizz()
        {
            // Check the third condition, return true when 3 % 3 = 0
            Assert.AreEqual("Fizz", MathGameEngine.GameLoop(3));
        }

        [TestMethod]
        public void When5ReturnsBuzz()
        {
            // Check the third condition, return true when 5 % 5 = 0
            Assert.AreEqual("Buzz", MathGameEngine.GameLoop(5));
        }

        [TestMethod]
        public void When15ReturnsFizzBuzz()
        {
            // Check the third condition, return true when 15 % 3 or 5 = 0
            Assert.AreEqual("FizzBuzz", MathGameEngine.GameLoop(15));
        }

        [TestMethod]
        public void ReturnTrue()
        {
            //Just a check for functioning test implementation
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void ReturnTrueIfMatchesTxt()
        {
            //Checks values against provided values correct in txt files
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "MathGameNova.UnitTests.FizzBuzz.txt";

            var counter = 1;
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (var reader = new StreamReader(stream))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Assert.AreEqual(line, MathGameEngine.GameLoop(counter));
                    counter++;
                }
                reader.Close();
            }
        }

        [TestMethod]
        public void ReturnTrueWhenCudaRunsGracefully()
        {
            // Return true if Cuda is available
            Assert.AreEqual(0, MathGameEngine.LaunchCuda(3));
        }

        [TestMethod]
        public void ReturnTrueIfBothGameLoopsAreSame()
        {
            // Checks if the two engines provide the same result
            for (var i = 1; i <= 100; i++)
            {
                Assert.AreEqual(MathGameEngine.GameLoop(i), MathGameEngine.ReadableLoop(i));
            }
        }
    }
}