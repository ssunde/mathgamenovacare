﻿using System;
using System.Diagnostics;

namespace MathGameNova.dll
{
    public static class MathGameEngine
    {
        /// <summary>
        /// Checks if a given int value is divisible by 3,5 or both. Returns String Fizz for 3, Buzz for 5 and "FizzBuzz" for both (=15).
        /// Checks the expected case first for highest performance.
        /// </summary>
        /// <param name="val"></param>
        /// <returns>String</returns>
        public static string GameLoop(int val)
        {
            if (val % 3 != 0 && val % 5 != 0) { return val.ToString(); } else if (val % 3 == 0 ^ val % 5 == 0) { return val % 3 == 0 ? "Fizz" : "Buzz"; }
            return "FizzBuzz";
        }

        /// <summary>
        /// Checks if a given int value is divisible by 3,5 or both. Returns String Fizz for 3, Buzz for 5 and "FizzBuzz" for both.
        /// Checks all cases.
        /// </summary>
        /// <param name="val"></param>
        /// <returns>String</returns>
        public static string ReadableLoop(int val)
        {
            var returnString = "";
            bool fizz = val % 3 == 0;
            bool buzz = val % 5 == 0;
            // Bool seems more readable than var at a glance
            if (fizz) { returnString += "Fizz"; }
            if (buzz) { returnString += "Buzz"; }
            if (!fizz && !buzz) { returnString = val.ToString(); }
            //
            return returnString;
        }

        /// <summary>
        /// Launches a Cuda kernel on GPU 1. 3 Different kernels are compiled, 1 = 100; 2 = 1000; 3 = 10000;
        /// </summary>
        /// <param name="listSize"></param>
        /// <returns></returns>
        public static int LaunchCuda(int listSize)
        {
            //Start a external process for the Cuda C++ file
            var startInfo = new ProcessStartInfo
            {
                CreateNoWindow = false,
                UseShellExecute = false
            };
            //Kernels must be compiled with data atm, to prevent having to figure out external malloc() calls
            switch (listSize)
            {
                case 1:
                    startInfo.FileName = "Built Kernels\\app100.exe";
                    break;

                case 2:
                    startInfo.FileName = "Built Kernels\\app1000.exe";
                    break;

                case 3:
                    startInfo.FileName = "Built Kernels\\app10000.exe";
                    break;

                case 4:
                    startInfo.FileName = "Built Kernels\\app100000.exe";
                    break;
            }

            try
            {
                // Start the process we specified.
                // Call WaitForExit and then the using-statement will close.
                using (var exeProcess = Process.Start(startInfo))
                {
                    Console.SetCursorPosition(0, 5);
                    if (exeProcess == null) return 1;
                    exeProcess.WaitForExit();
                    return exeProcess.ExitCode;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error starting Cuda thread on GPU 1 :  {e}");
                return 1;
            }
        }
    }
}