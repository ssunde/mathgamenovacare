﻿using MathGameNova.dll;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace MathGameNova
{
    internal static class Program
    {
        private static List<string> _printLines;

        /// <summary>
        /// Main entry for our console app "FizzBuzz". No args
        /// </summary>
        /// <return>null</return>
        private static void Main()
        {
            while (true)
            {
                _printLines = new List<string>(16);
                if (Debugger.IsAttached)
                //If console window is attached (terminal) then show some graphics!
                {
                    PrintTopScreen();
                    PrintStartMessage();
                    var key = Console.ReadKey();
                    //Wait for user input
                    if (key.KeyChar == 's')
                    {
                        RunSingleWithPrint();
                    }
                    else
                    {
                        ClearConsoleLinesFiveToSix();
                        WriteToConsoleAtLine(0, 5, "Select how many numbers in list: 1) 100 2) 1,000 3) 10,000 4)100,000");
                        var keySelectNumbers = Console.ReadKey();
                        ClearConsoleLinesFiveToSix();
                        WriteLoadingTopScreen(30);
                        Console.ForegroundColor = ConsoleColor.Green;
                        switch (keySelectNumbers.KeyChar)
                        {
                            case '1':
                                MathGameEngine.LaunchCuda(1);
                                break;

                            case '2':
                                MathGameEngine.LaunchCuda(2);
                                break;

                            case '3':
                                MathGameEngine.LaunchCuda(3);
                                break;

                            case '4':
                                MathGameEngine.LaunchCuda(4);
                                break;
                        }

                        Console.ForegroundColor = ConsoleColor.White;
                        for (var i = 1; i <= 100; i++)
                        {
                            if (i == 30 || i == 60 || i == 80)
                            {
                                WriteLoadingTopScreen(i + 10);
                            }

                            AddLinesToConsole(MathGameEngine.GameLoop(i), MathGameEngine.ReadableLoop(i));
                        }

                        WriteLoadingTopScreen(100);
                        PrintResultMessage();
                    }

                    var keyRerun = Console.ReadKey();
                    if (keyRerun.KeyChar == 'q') return;
                    Console.Clear();
                    continue;
                }

                break;
            }
        }

        /// <summary>
        /// Clears Console Lines Five To Six
        /// </summary>
        private static void ClearConsoleLinesFiveToSix()
        {
            WriteToConsoleAtLine(0, 5, "                                                                           ");
            WriteToConsoleAtLine(0, 6, "                                                                           ");
            WriteToConsoleAtLine(0, 7, "                                                                           ");
        }

        /// <summary>
        /// Prints the expected output from FizzBuzz.
        /// No GPU or other tasks.
        /// </summary>
        private static void RunSingleWithPrint()
        {
            for (var i = 1; i <= 100; i++)
            // Use 1 to match File Reader txt
            {
                Console.WriteLine(MathGameEngine.GameLoop(i));
                // Gets a value from the faster game engine
                Console.WriteLine("Press any key to continue or r to start over . . .");
            }
        }

        /// <summary>
        /// Displays the
        /// </summary>
        /// <param name="loopLeft"></param>
        /// <param name="loopRight"></param>
        private static void AddLinesToConsole(string loopLeft, string loopRight)
        {
            _printLines.Insert(0, $"{loopLeft}  == {loopRight}");
            if (_printLines.Count > 16)
            {
                _printLines.RemoveAt(16);
            }

            WriteProgress(0);
        }

        /// <summary>
        /// Prints the top graphic to the console.
        /// Can be replaced by WriteLoadingTopScreen(0)
        /// </summary>
        private static void PrintTopScreen()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine();
            Console.WriteLine("#######################################################################################################################");
            Console.WriteLine("###################################################  FizzBuzz MathGame  ###############################################");
            Console.WriteLine("#######################################################################################################################");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Prints the start message "Press any key.."
        /// </summary>
        private static void PrintStartMessage()
        {
            Console.WriteLine("Press any key to start! or s for single file");
        }

        /// <summary>
        /// Write one update of (10) lines to the console.
        /// X parameter sets offset from left side of console.
        /// </summary>
        /// <param name="x"></param>
        private static void WriteProgress(int x)
        {
            var width = Console.WindowWidth;
            Console.ForegroundColor = ConsoleColor.Red;
            x = x % width;
            try
            {
                Console.Write("                                               ");
                for (var i = 0; i < _printLines.Count; i++)
                {
                    Console.SetCursorPosition(x, 26 - i);
                    Console.Write(_printLines[i] + "                      ");
                }

                Console.SetCursorPosition(x, 28);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Press any key to restart or q to quit . . .");
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Write a string to a given position in the console.
        /// </summary>
        /// <param name="x">The offset from left side of the console</param>
        /// <param name="y">The offset of ROWS from top of the console</param>
        /// <param name="wordString">The string being printed</param>
        private static void WriteToConsoleAtLine(int x, int y, string wordString)
        {
            // Console.WindowWidth = 10;  // this works.
            var width = Console.WindowWidth;
            x = x % width;
            try
            {
                Console.SetCursorPosition(x, y);
                Console.Write(wordString);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Writes the given percentage represented by the top graphic turning green.
        /// </summary>
        /// <param name="percentage"></param>
        private static void WriteLoadingTopScreen(int percentage)
        {
            var left = new StringBuilder();
            left.Clear();
            var right = new StringBuilder();
            right.Clear();
            var percentageAdjusted = Console.WindowWidth * (percentage / 100);
            var leftCharCount = percentageAdjusted;
            var rightCharCount = Console.WindowWidth - percentageAdjusted;
            for (var i = 0; i < leftCharCount - 1; i++)
            {
                left.Append("#");
            }
            for (var i = 0; i < rightCharCount - 1; i++)
            {
                right.Append("#");
            }
            Console.ForegroundColor = ConsoleColor.Green;
            WriteToConsoleAtLine(0, 1, left.ToString());
            WriteToConsoleAtLine(0, 2, left.ToString());
            WriteToConsoleAtLine(0, 3, left.ToString());
            Console.ForegroundColor = ConsoleColor.Red;
            WriteToConsoleAtLine(leftCharCount, 1, right.ToString());
            WriteToConsoleAtLine(leftCharCount, 2, right.ToString());
            WriteToConsoleAtLine(leftCharCount, 3, right.ToString());
            WriteToConsoleAtLine(51, 2, "  FizzBuzz MathGame  ");
        }

        /// <summary>
        /// This method prints the two results cpu bound test after testing them without console printing for a true performance test
        /// </summary>
        private static void PrintResultMessage()
        {
            var stopwatch = new Stopwatch();
            // To test the performance of the two loops.
            stopwatch.Start();

            for (var i = 1; i <= 100; i++)
            {
                MathGameEngine.GameLoop(i);
            }
            stopwatch.Stop();
            var time1 = stopwatch.Elapsed;
            stopwatch.Restart();

            for (var i = 1; i <= 100; i++)
            {
                MathGameEngine.ReadableLoop(i);
            }
            var time2 = stopwatch.Elapsed;
            Console.ForegroundColor = ConsoleColor.Blue;
            WriteToConsoleAtLine(0, 7, $"Time for fast CPU method: {time1} at 100 values ");
            WriteToConsoleAtLine(0, 9, $"Time for \"readable\" CPU method {time2} at 100 values");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}